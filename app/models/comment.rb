class Comment < ApplicationRecord
  belongs_to :user
  belongs_to :book
  RAIT = (1..5).to_a

  validates :rating, presence: true, numericality: true
  validates :comment, presence: true, length: { minimum: 3 }
  validates_numericality_of :rating, :greater_than => 0, :message => "должен быть больше чем 0"
  validates_numericality_of :rating, :less_than => 6, :message => "должен быть меньше чем 6"

end
