Rails.application.routes.draw do
  # resources :books, only: [:new, :create, :show]
  resources :books

  resources :authors
  devise_for :users
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  resources :categories, only: [:new, :create, :show]
  root "books#index"

  scope :books do
    resources :comments, only: [:new, :create, :destroy]
  end

end
