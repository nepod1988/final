class Book < ApplicationRecord
  belongs_to :user
  has_and_belongs_to_many :categories
  has_and_belongs_to_many :authors
  has_many :comment, dependent: :destroy

  has_attached_file :image, styles: { medium: "600x600>", thumb: "150x150>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/

  validates :name, presence: true,
            uniqueness: { case_sensitive: false }
  validates :image, presence: true
  validates :description, presence: true
  validates :authors, presence: true
  validates :categories, presence: true



end
