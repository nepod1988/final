ActiveAdmin.register Book do

 permit_params :status

 index do
  id_column
  selectable_column
  column :name
  column :user_id
  column :description
  column :status
  actions
 end

 show do
  attributes_table do

   row :name
   row :user_id
   row :description
   row :status

   tabs do
    tab 'Comments' do
     table_for book.comment do
      column('Comment') { |p| p.comment }
      column('Rating') { |p| p.rating }
     end
    end
   end

  end
  active_admin_comments
 end

end
