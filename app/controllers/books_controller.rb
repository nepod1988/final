class BooksController < InheritedResources::Base

  before_action :authenticate_user!, only: [:new, :create]


  def index
    @books = if params[:term]

               Book.where('name LIKE ?', "%#{params[:term]}%")
             else
               Book.where(status: true)
             end
  end

  def create

    @book = Book.new(book_params)
    @book.user_id = current_user.id
    if @book.save
      flash[:success] = "Книга добавлена! Она будет отображена после модерации"
      redirect_to books_path
    else
      render 'new'
    end
  end



  private

    def book_params
      params.require(:book).permit(:name, :image, :description, :term, category_ids: [], author_ids: [])
    end
end

