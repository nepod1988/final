class CommentsController < ApplicationController

  def create
    @comment = Comment.new(comment_params)
    @comment.user_id = current_user.id

        # узнаем, оставлял ли юзер коммент к этой книге ранее

    old_comment = Comment.where(book_id: @comment.book_id, user_id: current_user.id).count


    if old_comment != 0
      flash[:success] = "Сперва нужно удалить старый комментарий"
      redirect_to "/books/#{@comment.book_id}"

    else
      if @comment.save
        flash[:success] = "Комментарий добавлен"
        redirect_to "/books/#{@comment.book_id}"
      else
        flash[:error] = "Правильно заполните поля"
        redirect_to "/books/#{@comment.book_id}"
      end

    end


  end

  def destroy

    @comment = Comment.find(params[:id])
    if @comment.user_id == current_user.id

    @comment = Comment.destroy(params[:id])
    respond_to do |format|
      format.js {
        flash[:success] = "Комментарий удален"
        redirect_to "/books/#{@comment.book_id}"
      }
      format.html {

      }
    end

    else
      flash[:error] = "Нельзя удалить чужой отзыв"
      redirect_to "/books/#{@comment.book_id}"
    end

  end


  private

  def comment_params
    params.require(:comment).permit(:rating, :comment, :book_id)
  end

end
