class AuthorsController < InheritedResources::Base

  private

    def author_params
      params.require(:author).permit(:name, :status)
    end
end

